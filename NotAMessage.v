Require Import Global.

Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.

Definition not_a_message (i:interact) : Prop := match i with
  | Message _ _ _ => False
  | _ => True
end.

(** Implementation. *)

Definition not_a_messageb (i:interact):bool :=
  match i with
    | Message m x y => false
    | _ => true
  end.

Lemma not_a_messageb_prop: 
  forall i,
  not_a_messageb i = true -> not_a_message i.
Proof.
  intros i H.
  destruct i.
  - simpl in *. inversion H.
  - simpl in *. auto.
  - simpl in *. auto.
  - simpl in *. auto.
Qed.
