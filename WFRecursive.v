Require Import Global.

Require Import Coq.Lists.List.

Definition recenv := list rec.

Inductive wf_rec: recenv -> interact -> Prop :=
  | wf_rec_message:
    forall env m x y,
    wf_rec env (Message m x y)
  | wf_rec_choice:
    forall env r (bs:group),
    wf_rec_branch env bs ->
    wf_rec env (Choice r bs)
  | wf_rec_recursive:
    forall env r b,
    wf_rec_block (r::env) b ->
    wf_rec env (Recursive r b)
  | wf_rec_continue:
    forall env r,
    In r env ->
    wf_rec env (Continue r)
(*  | wf_rec_parallel:
    forall bs b env,
    (forall b', In b' bs -> wf_rec nil b') ->
    wf_rec env b ->
    wf_rec env ((Parallel bs)::b) *)
with wf_rec_block: list rec -> block -> Prop :=
  | wf_rec_block_nil:
    forall env,
    wf_rec_block env bnil
  | wf_rec_block_cons:
    forall env i b,
    wf_rec_block env b ->
    wf_rec env i ->
    wf_rec_block env (bcons i b)
with wf_rec_branch: list rec -> group -> Prop :=
  | wf_rec_branch_nil:
    forall env,
    wf_rec_branch env gnil
  | wf_rec_branch_cons:
    forall env b bs,
    wf_rec_branch env bs ->
    wf_rec_block env b ->
    wf_rec_branch env (gcons b bs).



(*
Lemma not_wf_rec_cons:
  forall env b i,
  ~ wf_rec env b ->
  ~ (wf_rec env (i::b)).
Proof.
intros.
intuition.
inversion H0.
- apply H in H3. assumption.
- apply H in H4. assumption.
- apply H in H5. assumption.
- apply H in H5. assumption.
(* par - apply H in H5. assumption. *)
Qed. 
*)

Lemma wf_rec_block_nil_dec:
  forall env,
  {wf_rec_block env bnil} + {~ wf_rec_block env bnil}.
Proof.
  intros.
  apply (left (wf_rec_block_nil env)).
Qed.

Ltac doneg Neg Hyp := apply Neg in Hyp; inversion Hyp.

Ltac doright := apply right; assumption.

Lemma wf_rec_block_cons_dec:
  forall (env : list rec) (i : interact),
  {wf_rec env i} + {~ wf_rec env i} ->
  forall b : block,
  {wf_rec_block env b} + {~ wf_rec_block env b} ->
  {wf_rec_block env (bcons i b)} + {~ wf_rec_block env (bcons i b)}.
Proof.
  intros.
  destruct H0.
  - destruct H.
   + apply (left (wf_rec_block_cons env i b w w0)).
   + assert (H: ~ wf_rec_block env (bcons i b) ).
     intuition.
     inversion H.
     doneg n H4.
     doright.
  - assert (Ha: ~ wf_rec_block env (bcons i b) ).
    intuition.
    inversion H0.
    doneg n H3.
    inversion H0.
    doneg b0 H4.
    doright.
Qed.

Lemma wf_rec_branch_nil_dec:
  forall env,
  {wf_rec_branch env gnil} + {~ wf_rec_branch env gnil}.
Proof.
  intros.
  apply (left (wf_rec_branch_nil env)).
Qed.

Lemma wf_rec_branch_cons_dec:
  forall (env : list rec) (b : block),
  {wf_rec_block env b} + {~ wf_rec_block env b} ->
  forall l : group,
  {wf_rec_branch env l} + {~ wf_rec_branch env l} ->
  {wf_rec_branch env (gcons b l)} + {~ wf_rec_branch env (gcons b l)}.
Proof.
  intros.
  destruct H0.
  - destruct H.
   + apply (left (wf_rec_branch_cons env b l w w0)).
   + assert (H: ~ wf_rec_branch env (gcons b l) ).
     intuition.
     inversion H.
     doneg n H4.
     doright.
  - assert (Ha: ~ wf_rec_branch env (gcons b l) ).
    intuition.
    inversion H0.
    doneg n H3.
    inversion H0.
    doneg b0 H4.
    doright.
Qed.


Lemma wf_rec_message_dec:
forall env m x y,
{wf_rec env (Message m x y)} + {~ wf_rec env (Message m x y)}.
Proof.
  intros.
  apply (left (wf_rec_message env m x y)).
Qed.

Lemma wf_rec_choice_dec:
  forall env l,
  {wf_rec_branch env l} + {~ wf_rec_branch env l} ->
  forall r,
  {wf_rec env (Choice r l)} + {~ wf_rec env (Choice r l)}.
Proof.
  intros.
  destruct H.
  + apply (left (wf_rec_choice env r l w)).
  + assert (H: ~ wf_rec env (Choice r l)).
    intuition.
    inversion H.
    apply n in H2.
    inversion H2.
    right.
    assumption.
Qed.

(********* here be dragons ***************)

Lemma wf_rec_block_weak:
  forall x env b,
  wf_rec_block env b ->
  wf_rec_block (x::env) b.
Proof.
Admitted. 

Lemma foo:
forall env r b,
~ wf_rec_block env b ->
{wf_rec_block (r::env) b } + {~ wf_rec_block (r::env) b}.
Proof.
intros env r.
apply (block_rec3
  (fun i':interact => {wf_rec (r::env) i'} + {~ wf_rec (r::env) i'})
  (fun b:block => ~ wf_rec_block env b -> {wf_rec_block (r::env) b} + {~ wf_rec_block (r::env) b})
  (fun l:group => {wf_rec_branch (r::env) l} + {~ wf_rec_branch (r::env) l})
).
- intros m x y.
  apply (left (wf_rec_message (r::env) m x y)).
- intros a g IH.
  destruct IH.
  + apply ( left (wf_rec_choice (r::env) a g w)).
  + assert (Hc: ~ wf_rec (r::env) (Choice a g)).
    intuition.
    inversion H.
    doneg n H2.
    doright.
-intros r' b' Hb. (*
 assert (H: ~ wf_rec (r :: env) (Recursive r' b')).
 intuition.
 inversion H.
 + assert (H:= wf_rec_block_weak r' (r::env) b' w).
   apply wf_rec_recursive in H.
   left.
   assumption.
 + assert (Hc: wf_rec (r::env) (Recursive r' b')).
   intuition.*)
Admitted.

Lemma wf_rec_weak:
  forall x env i,
  wf_rec env i ->
  wf_rec (x::env) i.
Proof.
Admitted. 


Lemma neg_wf_rec_block_weak:
  forall x env b,
  ~ wf_rec_block (x::env) b ->
  ~ wf_rec_block env b.
Proof.
Admitted. 

Lemma case_rec_2:
  forall env b r,
  ~ wf_rec_block env b ->
  {wf_rec env (Recursive r b)} + {~ wf_rec env (Recursive r b)}.
Proof.
intros.
Admitted.

Lemma wf_rec_recursive_dec:
  forall env b,
  {wf_rec_block env b} + {~ wf_rec_block env b} ->
  forall r : rec, {wf_rec env (Recursive r b)} + {~ wf_rec env (Recursive r b)}.
Proof.
  intros.
  destruct H.
  + apply (left
      (wf_rec_recursive env r b (wf_rec_block_weak r env b w))
    ).
  + assert (H: ~ wf_rec env (Recursive r b)).
    intuition.
    inversion H.
    induction b.
    rewrite H3 in IHb. clear H1 H0 H3 r0 b0 env0.
Admitted.

Lemma wf_rec_continue_dec:
  forall env r,
  {wf_rec env (Continue r)} + {~ wf_rec env (Continue r)}.
Proof.
  intros.
Admitted.


Definition wf_rec_dec:
  forall env i,
  {wf_rec env i} + {~ wf_rec env i}.
Proof.
intros.
apply (interact_rec3
  (fun i':interact => {wf_rec env i'} + {~ wf_rec env i'})
  (fun b:block => {wf_rec_block env b} + {~ wf_rec_block env b})
  (fun l:group => {wf_rec_branch env l} + {~ wf_rec_branch env l})
).
- apply wf_rec_message_dec.
- apply wf_rec_choice_dec.
- apply wf_rec_recursive_dec.
- apply wf_rec_continue_dec.
- apply wf_rec_block_nil_dec.
- apply wf_rec_block_cons_dec.
- apply wf_rec_branch_nil_dec.
- apply wf_rec_branch_cons_dec.
Qed.
