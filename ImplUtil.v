Require Import Global.
Require Import WFChoice.

Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.


Definition receives_in_all_branches (r:role) (g:group) :=
  forall b, In b (gtolist g) -> receives r b.

Definition receiver (g:group) (p:role) :=
  exists b, In b (gtolist g) /\ receives p b.

Definition receiver_in_all_branches (r:role) (g:group) (p:role) :=
  receiver g p ->
  r <> p ->
  receives_in_all_branches p g.

Fixpoint receives_in_all_branchesb (r:role) (g:group) : bool :=
  match g with
    | gcons b g' => andb (receivesb r b) (receives_in_all_branchesb r g')
    | gnil => true
  end.

Lemma receives_in_all_branches_cons:
  forall r g b,
  receives r b ->
  receives_in_all_branches r g ->
  receives_in_all_branches r (gcons b g).
Proof.
Admitted.

Lemma receives_in_all_branchesb_prop:
  forall r g,
  receives_in_all_branchesb r g = true ->
  receives_in_all_branches r g.
Proof.
  intros r g H_eq.
  induction g.
  - simpl in *.
    apply andb_prop in H_eq.
    destruct H_eq as (H_eq1, H_eq2).
    apply IHg in H_eq2. clear IHg.
    apply receivesb_prop in H_eq1.
    apply receives_in_all_branches_cons.
    assumption.
    assumption.
  - unfold receives_in_all_branches.
    intros b H_in.
    inversion H_in.
Qed.

Definition receiverb (g:group) (p:role): bool :=
  existsb (receivesb p) (gtolist g).

Lemma receiverb_prop:
  forall g p,
  receiverb g p = true ->
  receiver g p.
Proof.
  intros g p H_r.
  unfold receiverb in H_r. 
  unfold receiver.
  rewrite existsb_exists in H_r.
  destruct H_r.
  exists x.
  destruct H as (H1,H2).
  apply receivesb_prop in H2.
  auto.
Qed.

Lemma receiver_to_b:
  forall g p,
  receiver g p ->
  receiverb g p = true.
Proof.
  intros g p H_r.
  unfold receiver in H_r.
  unfold receiverb.
  rewrite existsb_exists.
  destruct H_r.
  exists x.
  destruct H as (H1, H2).
  split.
  assumption.
  apply receives_to_b.
  assumption.
Qed.

Definition receiver_in_all_branchesb (r:role) (g:group) (p:role): bool :=
  implb (receiverb g p)
  (if role_dec r p then true else receives_in_all_branchesb p g).

Lemma receiver_in_all_branchesb_prop:
  forall r g p,
  receiver_in_all_branchesb r g p = true ->
  receiver_in_all_branches r g p.
Proof.
  intros r g p H.
  unfold receiver_in_all_branchesb in H.
  unfold receiver_in_all_branches.
  intros H_r H_neq.
  apply receiver_to_b in H_r.
  rewrite H_r in H.
  simpl in H. clear H_r.
  rewrite neq_role in H.
  apply receives_in_all_branchesb_prop.
  assumption.
  assumption.
Qed.

Definition all_roles (g:group) :=
  flat_map participants (gtolist g).

Lemma receives_in:
  forall p b,
  receives p b ->
  In p (participants b).
Proof.
  intros p b H_r.
  induction b.
  - inversion H_r.
    + rewrite <- H1 in *. clear H1 H2 H.
      simpl in *.
      auto.
    + apply IHb in H1.
      destruct i.
      simpl in *.
      auto.
      simpl in *.
      auto.
      simpl in *.
      auto.
      simpl in *.
      auto.
  - inversion H_r.
Qed.

Lemma receiverb_in:
  forall g p,
  receiverb g p = true ->
  In p (all_roles g).
Proof.
  intros g p H_r.
  unfold all_roles.
  rewrite in_flat_map.
  unfold receiverb in H_r.
  rewrite existsb_exists in H_r.
  destruct H_r.
  exists x.
  destruct H as (H1, H2).
  apply receivesb_prop in H2.
  split.
  - assumption.
  - clear H1.
    apply receives_in.
    assumption.
Qed.

Lemma nin_receiverb:
  forall g p,
  ~ In p (all_roles g) ->
  receiverb g p = false.
Proof.
  intros g p H_n.
  destruct (sumbool_of_bool (receiverb g p)).
  - apply receiverb_in in e.
    contradiction H_n.
  - assumption.
Qed.

Lemma nin_receiver_in_all_branchesb:
  forall r g p,
  ~ In p (all_roles g) ->
  receiver_in_all_branchesb r g p = true.
Proof.
  intros r g p H_nin.
  unfold receiver_in_all_branchesb.
  apply nin_receiverb in H_nin.
  rewrite H_nin.
  auto.
Qed.

