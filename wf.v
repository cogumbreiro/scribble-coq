
Parameter role : Type.

Parameter message : Type.

Parameter recvar : Type.

Inductive ginteract :=
  | GMessage: role -> role -> ginteract
  | GChoice: role -> list gblock -> ginteract
  | GInterrupt: ginterrupt -> ginteract
  | GMessageTransfer: role -> message -> list role -> ginteract
  | GRecursion: recvar -> gblock -> ginteract
  | GInterruptible: gblock -> list ginterrupt -> ginteract
with gblock := | gblock_def: list ginteract -> gblock
with ginterrupt := | ginterrupt_def: role -> list message -> ginterrupt.

Parameter env : Type.

Parameter is_enabled : env -> role -> Prop.

Parameter message_map: Type.

Parameter get_enabled : env -> message_map.

Parameter is_in: env -> gblock -> list gblock -> Prop.

Parameter set_of_roles: Type.

Parameter roles: env -> gblock -> set_of_roles.

Parameter DUMMY_ROLE : role.

Parameter add: env -> role -> role -> env.

Parameter merge_all: env -> list gblock -> env.

Parameter merge: env -> env -> env.

Inductive wf_choice: env -> ginteract -> env -> Prop :=
  | wf_choice_choice:
    forall e e' r b,
    e' = (add e DUMMY_ROLE r) ->
    is_enabled e' r -> 
    (forall x y,
      is_in e' x b ->
      is_in e' y b ->
      roles e' x = roles e' y) -> 
    wf_choice e (GChoice r b) (merge e' (merge_all e' b))
  | wf_choice_interrupt:
    forall e r m,
    is_enabled e r ->
    wf_choice e (GInterrupt (ginterrupt_def r m)) e
  | wf_choice_message_transfer:
    forall e r m rs,
    is_enabled e r ->
    wf_choice e (GMessageTransfer r m rs) e
