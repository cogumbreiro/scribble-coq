Require Import SetUtil.
Require Import OptionUtil.
Require Import Global.
Require Import WFChoice.
Require Import SendsFirst.
Require Import Receives.

Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.

(* Automatically check if the length of the group is ok *)
Ltac assert_length H g :=
  (assert (H : length (gtolist g) > 1); compute; auto).

(** Requirement 1. *)

Fixpoint requirement1b (r:role) (g:group) :bool :=
  match g with
    | gcons _ g' => true
    | gnil => false
  end.

Lemma requirement1b_prop:
  forall r g,
  requirement1b r g = true ->
  requirement1 r g.
Proof.
  intros r g H.
  destruct g.
  - unfold requirement1.
    simpl.
    auto with *.
  - inversion H.
Qed.

(** Requirement 2 *)

Definition requirement2b (r:role) (g:group) := 
  gforallb (fun b:block =>
    pforallb (fun p=>
      if role_dec r p then true
      else negb (sends_firstb p b)) b
  ) g.

Lemma requirement2b_prop:
  forall r g,
  requirement2b r g = true ->
  requirement2 r g.
Proof.
  intros r g H.
  unfold requirement2b in H.
  rewrite gforallb_forall in H.
  unfold requirement2.
  intros p b H_b H_p H_neq.
  assert (H1 := H b H_b). clear H.
  rewrite pforallb_forall in H1.
  assert (H2 := H1 p H_p); clear H1.
  rewrite neq_role in H2.
  rewrite negb_true_iff in H2.
  rewrite sends_first_eq_sends_firstb.
  intuition.
  rewrite H2 in H.
  inversion H.
  assumption.
Qed.

(** Requirement 3 *)

Definition requirement3b (r:role) (g:group): bool :=
  gforallb
  (fun b1:block =>
    gforallb (fun b2:block =>
      set_eqb role role_dec (participants b1) (participants b2)
    ) g
  )
  g.

Lemma requirement3b_prop:
  forall r g,
  requirement3b r g = true ->
  requirement3 r g.
Proof.
  intros.
  unfold requirement3b in H.
  rewrite gforallb_forall in H.
  unfold requirement3.
  intros.
  assert (H' := H _ H0); clear H.
  rewrite gforallb_forall in H'.
  assert (H := H' _ H1); clear H'.
  apply set_eqb_prop in H.
  assumption.
Qed.

(** Implementation of the typechecker *)

Definition wf_choiceb (i:interact) : bool :=
  match i with
    | Choice r g =>
      (requirement1b r g) &&
      (requirement2b r g) &&
      (requirement3b r g)
    | _ => true
  end.

Lemma wf_choiceb_prop :
  forall i,
  wf_choiceb i = true ->
  wf_choice i.
Proof.
  intros i H.
  destruct i.
  - apply wf_choice_skip.
    unfold not_a_choice.
    auto.
  - unfold wf_choiceb in H.
    apply andb_prop in H.
    destruct H as (H, H3).
    apply andb_prop in H.
    destruct H as (H1, H2).
    apply requirement1b_prop in H1.
    apply requirement2b_prop in H2.
    apply requirement3b_prop in H3.
    apply valid_requirements.
    assumption.
    assumption.
    assumption.
  - apply wf_choice_skip.
    unfold not_a_choice.
    auto.
  - apply wf_choice_skip.
    unfold not_a_choice.
    auto.
Qed.

(* Requirement 4 *)
(*
Definition requirement4 (r:role) (g:group) :=
  forall p b1 b2 q,
  In b1 (gtolist g) ->
  In b2 (gtolist g) ->
  In p (participants b1) -> 
  r <> p ->
  FirstFrom p b1 (m1, q) ->
  FirstFrom p b2 (m2, q).
*)

Definition requirement4b (r:role) (g:group) := 
gforallb (
  fun (b1:block) =>
    gforallb (
      fun (b2:block) =>
        forallb (
          fun (p:role) =>
          if role_dec r p then true
          else
          match receives_first p b1 with
            | Some q =>
              match receives_first p b2 with
                | Some q' =>  if role_dec q q' then true else false
                | None => true
              end
            | None => true
          end
        ) (participants b1)
    ) g
  ) g.

Lemma requirement4b_prop:
  forall r g,
  requirement4b r g = true ->
  requirement4 r g.
Proof.
  intros.
  unfold requirement4b in H.
  unfold requirement4.
  intros.
  rewrite gforallb_forall in H.
  assert (H':= H b1 H0). clear H.
  rewrite gforallb_forall in H'.
  assert (H:= H' b2 H1). clear H'.
  rewrite forallb_forall in H.
  assert (H':= H p H2). clear H.
  rewrite neq_role in H'.
  destruct (option_inv (receives_first p b1)).
  destruct H.
  rewrite H in H'.
  apply receives_first_eq in H5.
  rewrite H5 in H'.
  destruct (role_dec x q').
  rewrite e in *.
  apply receives_first_eq in H4.
  rewrite H4 in H.
  inversion H.
  reflexivity.
  inversion H'.
  apply receives_first_eq in H4.
  rewrite H4 in H.
  inversion H.
  assumption.
Qed.
