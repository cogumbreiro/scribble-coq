Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.
Require Import Coq.Lists.ListSet.

Require Import OptionUtil.

Definition set_eq {A: Type} (l1:list A) (l2:list A) :=
  incl l1 l2 /\ incl l2 l1.

Section SetEquality.
  Variable A:Type.

  Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.
  
  Definition mem := @set_mem A eq_dec.

  Lemma mem_prop:
    forall x l,
    mem x l = true ->
    In x l.
  Proof.
    apply set_mem_correct1.
  Qed.

  Fixpoint inclb (l1:list A) (l2:list A) :=
    match l1 with
      | cons x l1' => mem x l2 && inclb l1' l2
      | nil => true
    end.

  Lemma inclb_prop:
    forall l1 l2,
    inclb l1 l2 = true ->
    incl l1 l2.
  Proof.
    intros.
    induction l1.
    - unfold incl.
      intros.
      inversion H0.
    - simpl in H.
      apply andb_prop in H.
      destruct H as (H1, H2).
      apply IHl1 in H2. clear IHl1.
      apply mem_prop in H1.
      apply incl_cons.
      assumption.
      assumption.
  Qed.

  Definition set_eqb (l1:list A) (l2:list A) :=
    inclb l1 l2 && inclb l2 l1.

  Lemma set_eqb_prop:
    forall l1 l2,
    set_eqb l1 l2 = true ->
    set_eq l1 l2.
  Proof.
    intros.
    unfold set_eqb in H.
    apply andb_prop in H.
    destruct H as (H1, H2).
    apply inclb_prop in H1.
    apply inclb_prop in H2.
    unfold set_eq.
    auto.
  Qed.


  Definition Intersection (x:list A)( y:list A) (z:list A) :=
    forall e, In e z <-> (In e x /\ In e y).

  Definition set_inter := set_inter eq_dec.

  Lemma set_inter_prop:
    forall x y z,
    set_inter x y = z -> Intersection x y z.
  Proof.
    intros.
    unfold Intersection.
    intros.
    split.
    - intros.
      rewrite <- H in H0.
      assert (H1 := H0).
      apply (set_inter_elim1 eq_dec) in H0.
      apply (set_inter_elim2 eq_dec) in H1.
      auto.
    - intros.
      destruct H0 as (H1,H2).
      rewrite <- H.
      apply set_inter_intro.
      assumption.
      assumption.
  Qed.

(*
  Lemma set_inter_to_b:
    forall x y z,
    Intersection x y z -> set_inter x y = z.
  Proof.
    intros.
    unfold Intersection in H.
    induction x.
    - simpl.
      destruct z.
      reflexivity.
      assert (H1 := H a).
      assert (H2 : In a (a :: z)).
      simpl. left. auto.
      rewrite H1 in H2.
      inversion H2.
      inversion H0.
    - (* assert (H0 := H a); clear H. *)
      (* simpl. *)
      assert (H1 : forall e : A, In e z <-> In e x /\ In e y).
      clear IHx.



      destruct (bool_dec (set_mem eq_dec a y) true).
      assert (H_a_y := e); apply set_mem_correct1 in H_a_y.
      simpl.
      rewrite e in *.
      assert (H1 : forall e : A, In e z <-> In e x /\ In e y).
      clear IHx.
      intros.
      destruct (H e).
      split.
      intros.
      apply H0 in H2; clear H0.
      
      
      rewrite 
      destruct (set_In_dec eq_dec a z).
      assert (H_z := s).
      rewrite H0 in s.
      destruct s.
      assert (H2 := set_inter_intro eq_dec a _ _ H H1).
      assumption.
      
      assert (H3 : forall e : A, In e z <-> In e x /\ In e y); clear IHx.
      + intros.
        split.
        intros.
        rewrite H in H0.
        assert (H1 := H e H0); clear H.
        destruct (eq_dec e a).
        destruct H1 as (H1,H2).
        rewrite e0 in *.
        auto.
        
      split.
      assumption.
      apply H.
      assumption.

        destruct (set_In_dec eq_dec a z).
      + 
        assert (H1 := H e H0); clear H.
        split.
        
        assert (H1 := H a s); clear H.
        destruct H1 as (H1, H2).
        assert (H3 := IHx a).
     assert (H1 := IHx H); clear IHx.
     destruct z.
      + destruct y.
        simpl.
        set_inter
  Qed.
*)
  Definition set_disjoint (x:list A) (y:list A) := 
    set_inter x y = nil.
  

  Definition Disjoint (x:list A) (y:list A) :=
    (forall e, In e x -> ~ In e y) /\ (forall e, In e y -> ~ In e y).
    
End SetEquality.

