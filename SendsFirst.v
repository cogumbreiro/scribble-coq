Require Import Global.
Require Import NotAMessage.

Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.

Inductive sends_first: role -> block -> Prop :=
  | sends_first_ok:
    forall x y m b,
    x <> y ->
    sends_first x (bcons (Message m x y) b)
  | sends_first_cons:
    forall x b i,
    sends_first x b ->
    not_a_message i ->
    sends_first x (bcons i b)
  | sends_first_skip:
    forall r m x y b,
    r <> x ->
    r <> y ->
    sends_first r b ->
    sends_first r (bcons (Message m x y) b).

(** Implementation *)

Fixpoint sends_firstb (r:role) (b:block): bool :=
  match b with
    | bcons i b' =>
      match i with
        | Message m x y =>
          if role_dec r y then false else  
          if role_dec r x then true
          else sends_firstb r b'
        | _ => sends_firstb r b'
      end
    | bnil => false
  end.

Lemma bsends_ok:
  forall r m x y b,
  sends_firstb r (bcons (Message m x y) b) = true ->
  y <> r /\ (x = r \/ sends_firstb r b = true).
Proof.
  intros r m x y b H.
  simpl in H.
  destruct (role_dec r y).
  inversion H.
  destruct (role_dec r x).
  split.
  auto.
  auto.
  split.
  auto.
  auto.
Qed.

Definition bnot_a_message (i:interact):bool :=
  match i with
    | Message m x y => false
    | _ => true
  end.

Lemma bnot_a_message_to_prop: 
  forall i,
  bnot_a_message i = true -> not_a_message i.
Proof.
  intros i H.
  destruct i.
  - simpl in *. inversion H.
  - simpl in *. auto.
  - simpl in *. auto.
  - simpl in *. auto.
Qed.

Lemma simpl_impl:
  forall P,
  (true = true -> P) -> P.
Proof.
intros P.
intros H.
assert (H': true = true).
reflexivity.
apply H in H'.
assumption.
Qed.

Lemma sends_firstb_prop :
  forall r b,
  sends_firstb r b = true -> sends_first r b.
Proof.
  intros r b H.
  induction b.
  - destruct i.
    + destruct (bsends_ok _ _ _ _ _ H) as (H1,H2).
      destruct H2 as [H2|H2].
      rewrite H2 in *. clear H2.
      apply sends_first_ok.
      auto.
      apply IHb in H2. clear IHb.
      destruct (role_dec r r0).
      rewrite e in *. clear e.
      apply sends_first_ok.
      auto.
      apply sends_first_skip.
      assumption.
      auto.
      assumption.
    + simpl in H. apply IHb in H.
      assert (H1 := bnot_a_message_to_prop (Choice r0 g)).
      apply simpl_impl in H1.
      apply (sends_first_cons r b (Choice r0 g) H H1).
    + simpl in H. apply IHb in H.
      assert (H1 := bnot_a_message_to_prop (Recursive r0 b0)).
      apply simpl_impl in H1.
      apply (sends_first_cons r b (Recursive r0 b0) H H1).
    + simpl in H. apply IHb in H.
      assert (H1 := bnot_a_message_to_prop (Continue r0)).
      apply simpl_impl in H1.
      apply (sends_first_cons r b (Continue r0) H H1).
 - compute in H. inversion H.
Qed.

Lemma sends_first_to_b:
  forall r b,
  sends_first r b -> sends_firstb r b = true.
Proof.
  intros r b H.
  induction b.
  - simpl.
    inversion H.
    + rewrite (neq_role bool _ _ _ _ H2).
      rewrite (eq_role bool _ _ _).
      reflexivity.
    + apply IHb in H3. clear IHb.
      destruct i.
      inversion H4.
      assumption.
      assumption.
      assumption.
    + rewrite (neq_role bool _ _ _ _ H2).
      rewrite (neq_role bool _ _ _ _ H4).
      apply IHb in H5.
      assumption.
  - inversion H.
Qed.

Lemma sends_first_eq_sends_firstb:
  forall r b,
  sends_first r b <-> sends_firstb r b = true.
Proof.
  split.
  apply sends_first_to_b.
  apply sends_firstb_prop.
Qed.
