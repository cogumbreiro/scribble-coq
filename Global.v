Require Import Coq.Lists.List.
Parameter role : Type.


Parameter role_dec:
  forall (x y : role),
  {x = y} + {x <> y}.

Parameter eq_role : forall (T:Type) x (p q:T), 
              (if role_dec x x then p else q) = p. 
Parameter neq_role : forall (T:Type) x y (p q:T), x <> y -> 
               (if role_dec x y then p else q) = q. 


Parameter message : Type.

Parameter rec: Type.

Parameter rec_dec:
  forall (x y : rec),
  {x = y} + {x <> x}.

(* global-interaction *)
Inductive interact :=
  | Message: message -> role -> role -> interact
  | Choice: role -> group -> interact
  | Recursive: rec -> block -> interact
  | Continue: rec -> interact
(*  | Parallel: list (list interact) -> interact *)
with block :=
  | bcons: interact -> block -> block
  | bnil: block
with group :=
  | gcons: block -> group -> group
  | gnil: group.

Scheme interact_ind3 := Induction for interact Sort Prop
  with block_ind3 := Induction for block Sort Prop
  with group_ind3 := Induction for group Sort Prop.

Combined Scheme mut_ind from interact_ind3, block_ind3, group_ind3.

Scheme interact_rec3 := Induction for interact Sort Set
  with block_rec3 := Induction for block Sort Set
  with group_rec3 := Induction for group Sort Set.

