Require Import SetUtil.
Require Import Global.
Require Import NotAMessage.
Require Import SendsFirst.
Require Import Receives.

Require Import Coq.Lists.List.
Require Import Coq.Bool.Bool.
Require Import Coq.Bool.BoolEq.
Require Import Coq.Bool.Sumbool.

Fixpoint participants (b:block): list role :=
   match b with
     | bnil => nil
     | bcons i b' =>
       let result := participants b' in
       match i with
         | Message _ x y => cons x (cons y result)
         | _ => result
       end
   end.

Definition not_a_choice (i:interact) : Prop := match i with
  | Choice _ _ => False
  | _ => True
end.

Fixpoint gtolist (g:group): list block :=
  match g with
    | gcons b g' => cons b (gtolist g')
    | gnil => nil
  end.

Fixpoint btolist (b:block): list interact :=
  match b with
    | bcons i b' => cons i (btolist b')
    | bnil => nil
  end.

(* There should be at least one block: n > 0. *)
Definition requirement1 (r:role) (g:group) :=
    length (gtolist g) > 0.

(** Within each blocki, for 1 ≤ i ≤ n,
    there is an occurrence of a role R′ as the receiver of
    a global-message-transfer at some point prior to any
    occurrence of R′ that is not the receiver of a
    global-message-transfer. *)
Definition requirement2 (r:role) (g:group) :=
  forall p b,
  In b (gtolist g) -> 
  In p (participants b) ->
  r <> p ->
  ~ sends_first p b.

(* We also need that each B (that is not A) receives its
   first message from the same role R in each block. *)
Definition requirement4 (r:role) (g:group) :=
  forall p b1 b2 q q',
  In b1 (gtolist g) ->
  In b2 (gtolist g) ->
  (* next premise is unimportant, but helps guide the implementation *)
  In p (participants b1) -> 
  r <> p ->
  ReceivesFirst p b1 q ->
  ReceivesFirst p b2 q' ->
  q = q'.
  (* We can even reason about the messages themselves:
  m1 <> m2. *)
(* XXX *)

(* Within each blocki, for 1 ≤ i ≤ n,
   there is an occurrence of a role R′ as the receiver of
   a global-message-transfer at some point prior to any
   occurrence of R′ that is not the receiver of a 
   global-message-transfer. *)
Definition requirement3 (r:role) (g:group) :=
  forall b1 b2,
  In b1 (gtolist g) ->
  In b2 (gtolist g) ->
  set_eq (participants b1) (participants b2).

Inductive wf_choice : interact -> Prop :=
  | wf_choice_choice:
    forall (A:role) (g:group),
    (* Requirement 1 *)
    length (gtolist g) > 0 ->
    (* Requirement 2 *)
    (forall p b,
      In b (gtolist g) -> 
      In p (participants b) ->
      A <> p ->
      ~ sends_first p b) ->
    (* Requirement 3 *)
    (forall b1 b2,
      In b1 (gtolist g) ->
      In b2 (gtolist g) ->
      set_eq (participants b1) (participants b2)) ->
    wf_choice (Choice A g)
  | wf_choice_skip:
    forall i,
    not_a_choice i ->
    wf_choice i

with wf_choice_block : block -> Prop :=
  | wf_choice_block_cons:
    forall b i,
    wf_choice i ->
    wf_choice_block b ->
    wf_choice_block (bcons i b)
  | wf_choice_block_nil:
    wf_choice_block bnil

with wf_choice_group : group -> Prop :=
  | wf_choice_group_cons:
    forall b g,
    wf_choice_block b ->
    wf_choice_group g ->
    wf_choice_group (gcons b g)
  | wf_choice_group_nil:
    wf_choice_group gnil.

(* Sanity check that all the requirements are well defined. *)
Lemma valid_requirements:
  forall r g,
  requirement1 r g ->
  requirement2 r g ->
  requirement3 r g ->
  wf_choice (Choice r g).
Proof.
  intros.
  unfold requirement1 in H.
  unfold requirement2 in H0.
  unfold requirement3 in H1.
  apply wf_choice_choice.
  assumption.
  assumption.
  assumption.
Qed.

(** Utility function *)
 
Fixpoint gforallb (f: block -> bool) (g:group): bool :=
  match g with
    | gcons b g' => andb (f b) (gforallb f g')
    | gnil => true
  end.

Lemma gforallb_cons:
  forall f b g,
  f b = true ->
  gforallb f g = true ->
  gforallb f (gcons b g) = true.
Proof.
  intros f b g H1 H2.
  simpl.
  assert (H3: f b = true /\ gforallb f g = true).
  auto.
  apply andb_true_intro in H3.
  assumption.
Qed.

Lemma gforallb_inv1:
  forall f b g,
  gforallb f (gcons b g) = true ->
  gforallb f g = true.
Proof.
  intros r b g H.
  induction g.
  - simpl in H.
    apply andb_prop in H.
    destruct H as (H1, H2).
    apply andb_prop in H2.
    destruct H2 as (H2, H3).
    assert (H4 := gforallb_cons _ _ _ H1 H3).
    apply IHg in H4.
    assert (H5 := gforallb_cons _ _ _ H2 H4).
    assumption.
  - auto. 
Qed.

Lemma gforallb_eq_forallb:
  forall (f : block -> bool) (g : group),
  gforallb f g = true <-> forallb f (gtolist g) = true.
Proof.
  intros f g.
  split.
  - intros H_g.
    induction g.
    + simpl in H_g.
      apply andb_prop in H_g.
      destruct H_g as (H1,H2).
      apply IHg in H2.
      simpl.
      rewrite andb_true_iff.
      auto.
    + auto.
  - intros H.
    induction g.
    + simpl.
      rewrite andb_true_iff.
      simpl in H.
      rewrite andb_true_iff in H.
      destruct H as (H1,H2). 
      split.
      assumption.
      apply IHg in H2.
      assumption.
    + auto.
Qed.

Lemma gforallb_forall:
  forall (f : block -> bool) (g : group),
  gforallb f g = true <-> (forall b : block, In b (gtolist g) -> f b = true).
Proof.
  intros f g.
  rewrite gforallb_eq_forallb with (f:=f).
  apply forallb_forall.
Qed.

Section Helper.
  Variable f : block -> bool.
  Variable P : block -> Prop.
  Hypothesis f_prop: forall b:block, f b = true -> P b.
  Variable g : group.
  
Lemma gforallb_prop:
  gforallb f g = true -> (forall b : block, In b (gtolist g) -> P b).
Proof.
  rewrite gforallb_forall.
  intros H b H_in.
  assert (H' := H _ H_in).
  apply f_prop.
  assumption.
Qed.
End Helper.

Definition pforallb (f: role -> bool) (b:block) : bool :=
  forallb f (participants b).

Lemma pforallb_forall:
  forall (f : role -> bool) (b : block),
  pforallb f b = true <-> (forall p : role, In p (participants b) -> f p = true).
Proof.
  intros f b.
  unfold pforallb.
  apply forallb_forall.
Qed.


Fixpoint bforallb (f: interact -> bool) (b:block): bool :=
  match b with
    | bcons i b' => andb (f i) (bforallb f b')
    | bnil => true
  end.

Lemma bforallb_cons:
  forall f i b,
  f i = true ->
  bforallb f b = true ->
  bforallb f (bcons i b) = true.
Proof.
  intros f i b H1 H2.
  simpl.
  assert (H3: f i = true /\ bforallb f b = true).
  auto.
  apply andb_true_intro in H3.
  assumption.
Qed.

Lemma bforallb_inv1:
  forall f i b,
  bforallb f (bcons i b) = true ->
  bforallb f b = true.
Proof.
  intros r i b H.
  induction b.
  - simpl in H.
    apply andb_prop in H.
    destruct H as (H1, H2).
    apply andb_prop in H2.
    destruct H2 as (H2, H3).
    assert (H4 := bforallb_cons _ _ _ H1 H3).
    apply IHb in H4.
    assert (H5 := bforallb_cons _ _ _ H2 H4).
    assumption.
  - auto. 
Qed.

Lemma bforallb_eq_forallb:
  forall (f : interact -> bool) (b : block),
  bforallb f b = true <-> forallb f (btolist b) = true.
Proof.
  intros f g.
  split.
  - intros H_g.
    induction g.
    + simpl in H_g.
      apply andb_prop in H_g.
      destruct H_g as (H1,H2).
      apply IHg in H2.
      simpl.
      rewrite andb_true_iff.
      auto.
    + auto.
  - intros H.
    induction g.
    + simpl.
      rewrite andb_true_iff.
      simpl in H.
      rewrite andb_true_iff in H.
      destruct H as (H1,H2). 
      split.
      assumption.
      apply IHg in H2.
      assumption.
    + auto.
Qed.

Lemma bforallb_forall:
  forall (f : interact -> bool) (b : block),
  bforallb f b = true <-> (forall i : interact, In i (btolist b) -> f i = true).
Proof.
  intros f b.
  rewrite bforallb_eq_forallb with (f:=f).
  apply forallb_forall.
Qed.
(*
Section Helper2.
  Variable f : interact -> bool.
  Variable P : interact -> Prop.
  Hypothesis f_prop: forall i:interact, f i = true -> P i.
  Variable b : block.
  
Lemma bforallb_prop:
  bforallb f b = true -> (forall i : interact, In i (btolist b) -> P i).
Proof.
  rewrite bforallb_forall.
  intros H g H_in.
  assert (H' := H _ H_in).
  apply f_prop.
  assumption.
Qed.
End Helper2.
*)
