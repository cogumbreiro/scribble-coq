Lemma option_inv:
  forall {A:Type} (o:option A),
  (exists a:A, o = Some a) \/ o = None.
Proof.
  intros.
  destruct o.
  left; exists a.
  reflexivity.
  auto.
Qed.

