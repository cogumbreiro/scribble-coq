
Inductive sends : role -> block -> Prop :=
  | sends_ok: 
    forall x y m bs,
    sends x (bcons (Message m x y) bs)
  | sends_cons:
    forall x i bs,
    sends x bs ->
    sends x (bcons i bs).
