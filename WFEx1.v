Require Import Coq.Lists.List.

Require Import Global.
Require Import WFChoice.


(*
module base.choise.Choice1;

global protocol Choice1(role A, role B)
{
	choice at A
	{
		l1() from A to B;
	}
	or
	{
		//l1() from A to B;  // Uncomment is bad
		l2() from A to B;
	}
}
*)
Parameter A:role.
Parameter l1:message.
Parameter l2:message.
Parameter B:role.
Axiom A_diff_B : A <> B.
Lemma B_diff_A : B <> A.
Proof.
  apply (not_eq_sym (A_diff_B)).
Qed.
Lemma AB: forall (p q:bool), (if role_dec A B then p else q) = q.
Proof.
intros p q.
assert (X:=neq_role bool A B p q A_diff_B).
assumption.
Qed.
Lemma BA: forall (p q:bool), (if role_dec B A then p else q) = q.
Proof.
intros p q.
assert (X:=neq_role bool B A p q B_diff_A).
assumption.
Qed.
Lemma AA: forall (p q:bool), (if role_dec A A then p else q) = p.
Proof.
intros p q.
assert (Y:=eq_role bool A p q).
assumption.
Qed.
Lemma BB: forall (p q:bool), (if role_dec B B then p else q) = p.
Proof.
intros p q.
assert (Y:=eq_role bool B p q).
assumption.
Qed.
Ltac magic := compute; repeat (try rewrite AB;try rewrite AA;try rewrite BB;try rewrite BA); auto.

Definition m1 := Message l1 A B.
Definition m2 := Message l2 A B.
Definition br1 := bcons m1 bnil.
Definition br2 := bcons m2 bnil.
Definition body := gcons br1 (gcons br2 gnil).
Definition example1 := Choice A body.
Goal wf_choice example1.
assert (H : wf_choiceb example1 = true).
magic.
apply wf_choiceb_prop.
assumption.
Qed.
