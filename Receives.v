Require Import OptionUtil.
Require Import Global.
Require Import NotAMessage.

Definition sender := (message * role) % type.

Inductive FirstFrom: role -> block -> sender -> Prop :=
  | first_from_ok:
    forall r x m b,
    r <> x ->
    FirstFrom r (bcons (Message m x r) b) (m, x)
  | first_from_cons:
    forall x b i s,
    FirstFrom x b s ->
    not_a_message i ->
    FirstFrom x (bcons i b) s
  | first_from_skip:
    forall r m x y b s,
    r <> x ->
    r <> y ->
    FirstFrom r b s ->
    FirstFrom r (bcons (Message m x y) b) s.

Definition ReceivesFirst (r:role) (b:block) (p:role) :=
  exists m, FirstFrom r b (m, p).

Definition Receives (r:role) (b:block) :=
  exists p, ReceivesFirst r b p.

(** Implementation *)

Fixpoint first_from (r:role) (b:block) : option sender :=
  match b with
    | bcons i b' =>
      match i with
        | Message m x y =>
          if role_dec r x then None
          else if role_dec r y then Some (m, x)
          else first_from r b'
        | _ => first_from r b'
      end
    | bnil => None
  end.

Definition receives_first (r:role) (b:block) : option role :=
  match first_from r b with
    | Some s => Some (snd s)
    | None => None
  end.

Definition receives (r:role) (b:block) :=
  match receives_first r b with
    | Some _ => true
    | None => false
  end.

Lemma first_from_of_inv:
  forall r m x y b s,
  first_from r (bcons (Message m x y) b) = Some s ->
  (x <> r /\ ((y = r /\ s = (m, x)) \/ (y <> r /\ first_from r b = Some s))).
Proof.
  intros.
  simpl in H.
  destruct (role_dec r x).
  - inversion H.
  - apply not_eq_sym in n.
    destruct (role_dec r y).
    split.
    assumption.
    left.
    auto.
    split.
    symmetry.
    assumption.
    inversion H.
    reflexivity.
    auto.
Qed.

Lemma simpl_impl:
  forall P,
  (true = true -> P) -> P.
Proof.
intros P.
intros H.
assert (H': true = true).
reflexivity.
apply H in H'.
assumption.
Qed.

Lemma first_from_prop:
  forall r b s,
  first_from r b = Some s -> FirstFrom r b s.
Proof.
  intros.
  induction b.
  - destruct i.
    + destruct (first_from_of_inv _ _ _ _ _ _ H) as (H1,H2).
      destruct H2 as [H2|H2].
      destruct H2 as (H2,H3).
      rewrite H2 in *. clear H2.
      rewrite H3 in *. clear H3.
      apply first_from_ok.
      auto.
      destruct H2 as (H3,H2).
      apply IHb in H2. clear IHb.
      apply first_from_skip.
      auto.
      auto.
      assumption.
    + simpl in H. apply IHb in H.
      assert (H1 := not_a_messageb_prop (Choice r0 g)).
      apply simpl_impl in H1.
      apply (first_from_cons _ _ _ _ H H1).
    + simpl in H. apply IHb in H.
      assert (H1 := not_a_messageb_prop (Recursive r0 b0)).
      apply simpl_impl in H1.
      apply (first_from_cons _ _ _ _ H H1).
    + simpl in H. apply IHb in H.
      assert (H1 := not_a_messageb_prop (Continue r0)).
      apply simpl_impl in H1.
      apply (first_from_cons _ _ _ _ H H1).
 - compute in H. inversion H.
Qed.

Lemma first_from_cons_not_msg:
  forall r i b s,
  first_from r b = Some s ->
  not_a_message i ->
  first_from r (bcons i b) = Some s.
Proof.
  intros.
  destruct i.
  - inversion H0.
  - auto.
  - auto.
  - auto.
Qed.


Lemma first_from_to_b:
  forall r b s,
  FirstFrom r b s -> first_from r b = Some s.
Proof.
  intros.
  induction b.
  - inversion H.
    + unfold first_from.
      rewrite neq_role.
      rewrite eq_role.
      reflexivity.
      assumption.
    + apply IHb in H3. clear IHb.
      apply first_from_cons_not_msg.
      assumption.
      assumption.
    + apply IHb in H6. clear IHb.
      simpl.
      rewrite neq_role.
      rewrite neq_role.
      assumption.
      assumption.
      assumption.
  - inversion H.
Qed.

Lemma first_from_eq:
  forall r b s,
  first_from r b = Some s <-> FirstFrom r b s.
Proof.
  intros.
  split.
  apply first_from_prop.
  apply first_from_to_b.
Qed.

Lemma first_from_det:
  forall r b s s',
  FirstFrom r b s ->
  FirstFrom r b s' ->
  s = s'.
Proof.
  intros.
  apply first_from_to_b in H.
  apply first_from_to_b in H0.
  rewrite H in H0.
  inversion H0.
  reflexivity.
Qed.

Lemma receives_first_eq:
  forall r b p,
  receives_first r b = Some p <-> ReceivesFirst r b p.
Proof.
  intros.
  unfold receives_first.
  unfold ReceivesFirst.
  split.
  assert (H:= option_inv (first_from r b)).
  destruct H.
  elim H. clear H.
  intros.
  rewrite H in H0.
  inversion H0.
  destruct x.
  rewrite first_from_eq in H.
  exists m.
  auto.
  intros.
  rewrite H in H0.
  inversion H0.
  intros.
  destruct H.
  apply first_from_eq in H.
  rewrite H.
  auto.
Qed.

Lemma receives_eq:
  forall r b,
  receives r b = true <-> Receives r b.
Proof.
  intros.
  split.
  + intros.
    unfold receives in H; unfold Receives.
    destruct (option_inv (receives_first r b)).
    - destruct H0.
      rewrite H0 in H.
      exists x.
      apply receives_first_eq.
      assumption.
    - rewrite H0 in H.
      inversion H.
  + intros.
    unfold receives; unfold Receives in H.
    destruct H.
    apply receives_first_eq in H.
    rewrite H.
    reflexivity.
Qed.